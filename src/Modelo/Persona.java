/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *  Clase representa una persona con su fecha de nacimiento
 * @author Jennifer Salazar, José Gabriel Fuentes
 */
public class Persona implements Comparable {
    
    private String nombresCompletos;
    //Su fecha de nacimiento
    private short dia, mes,agno, hora, min, seg;

    /**
     * Crea una persona vacía
     */
    public Persona(){
    }
    
    /**
     * Crea una persona, recibiendo sus parámetros
     */
    public Persona(String nombresCompletos, short dia, short mes, short agno, short hora, short min, short seg) throws Exception {
        this.nombresCompletos = nombresCompletos;
        this.dia = dia;
        this.mes = mes;
        this.agno = agno;
        this.hora = hora;
        this.min = min;
        this.seg = seg;
        
        this.validarFecha();
    }

    private void validarFecha() throws Exception {

        boolean validarDia, validarMes, validarAgno, validarHora, validarMin, validarSeg;
        validarDia = (this.dia > 0 && this.dia < 32);
        validarMes = (this.mes > 0 && this.mes < 13);
        validarAgno = (this.agno > 0 && this.agno < 2022);
        validarHora = (this.hora >= 0 && this.hora < 24);
        validarMin = (this.min >= 0 && this.min < 60);
        validarSeg = (this.seg >= 0 && this.seg < 60);
        
        if (!validarDia) {
            throw new Exception("Dato fuera de rango:" + this.dia);
        }
        if (!validarMes ) {
            throw new Exception("Dato fuera de rango:" + this.mes);
        }
        if (!validarAgno) {
            throw new Exception("Dato fuera de rango:" + this.agno);
        }
        if (!validarHora) {
            throw new Exception("Dato fuera de rango:" + this.hora);
        }
        if (!validarMin) {
            throw new Exception("Dato fuera de rango:" + this.min);
        }
        if (!validarSeg) {
            throw new Exception("Dato fuera de rango:" + this.seg);
        }

    }
    /**
     * Retorna los nombres completos de una persona
     *
     * @return un String con su respectivo nombre
     */
    public String getNombresCompletos(){
        return nombresCompletos;
    }
    
    /**
     * Actualiza el nombre completo de la persona
     *
     * @param nombresCompletos nuevo nombre para la persona
     */
    public void setNombresCompletos(String nombresCompletos){
        this.nombresCompletos = nombresCompletos;
    }

    public short getDia() {
        return dia;
    }

    public void setDia(short dia) {
        this.dia = dia;
    }

    public short getMes() {
        return mes;
    }

    public void setMes(short mes) {
        this.mes = mes;
    }

    public short getAgno() {
        return agno;
    }

    public void setAgno(short agno) {
        this.agno = agno;
    }

    public short getHora() {
        return hora;
    }

    public void setHora(short hora) {
        this.hora = hora;
    }

    public short getMin() {
        return min;
    }

    public void setMin(short min) {
        this.min = min;
    }

    public short getSeg() {
        return seg;
    }

    public void setSeg(short seg) {
        this.seg = seg;
    }
    
    @Override
    public String toString(){
        return this.nombresCompletos+"\nFecha de nacimiento: "+this.dia+"/"+this.mes+"/"+this.agno+""
                                     +"\nHora de nacimiento: "+this.hora+":"+this.min+":"+this.seg+"" ;
    }
    
    @Override
    public int hashCode(){
        
        int hash = 24;
        hash = 999*hash + Float.floatToIntBits(this.agno);
        hash = 999*hash + Float.floatToIntBits(this.mes);
        hash = 999*hash + Float.floatToIntBits(this.dia);
        hash = 999*hash + Float.floatToIntBits(this.hora);
        hash = 999*hash + Float.floatToIntBits(this.min);
        hash = 999*hash + Float.floatToIntBits(this.seg);

        return hash;
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) { 
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona obj2 = (Persona)obj;
        
        if (!this.nombresCompletos.equals(obj2.nombresCompletos)){
            return false;
        }
        return true;
    }
    
    
    @Override
    public int compareTo(Object obj){
        int compare = -1;
        
        if (obj == null) { 
            throw new RuntimeException("Debido a que el segundo objeto es nulo, no se pueden comparar");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("Los objetos no son de la misma clase");
        }
        final Persona obj2 = (Persona)obj;

        /**
         * Realizamos restas entre el año, mes y dia de nacimiento de los dos objetos a comparar,
         * para conocer la diferencia entre las fechas de nacimiento de ambas personas
         */
        int comparadorAgno = this.agno-obj2.agno;
        int comparadorMes = this.mes-obj2.mes;
        int comparadorDia = this.dia-obj2.dia;
        
        /**
         * Realizamos una conversión de las horas y los minutos a segundos, para que todo nos quede en los mismos 
         * valores(segundos) y sacar la diferencia entre la hora de nacimiento de una persona con la otra
         */
        long comparadorHorario = (this.hora*3600+this.min*60+this.seg)-(obj2.hora*3600+obj2.min*60+obj2.seg);
        
        /**
         * Vamos revisando si se cumplen las condiciones, en orden de jerarquia, para conocer si tienen o no tienen
         * el mismo año, mes, dia de nacimiento, o fecha de nacimiento como tal.
         */
         
        if(comparadorDia==0)
            compare = 6;
        if(comparadorMes==0){
            compare = 5;
            if(comparadorDia==0)
            compare = 9;// Si el mes y dia son iguales compare guardara el 9
        }
        if(comparadorAgno==0){
            compare = 4;    
            if(comparadorMes==0)
                compare = 7;// Si el año y mes son iguales compare guardara el 7
            else if(comparadorDia==0)
                compare = 8;// Si el año y dia son iguales compare guardara el 8
        }
        
        if(comparadorAgno!=0 && comparadorMes!=0 && comparadorDia!=0) 
            compare = 10;
        if(comparadorAgno==0 && comparadorMes==0 && comparadorDia==0){
            compare = 11;// Si el año, mes y dia son iguales compare guardara el 11
        }
        
        /**
         * Si se cumple el caso de que tengan exactamente la misma fecha de nacimiento, a continuación 
         * comprobaremos si tienen el mismo horario de nacimiento, o si una persona es mayor que otra por horario
         */
        if(compare==11){
            if((int)comparadorHorario==0)
                compare = 0;
            else if((int)comparadorHorario>0)
                compare = 1;
            else 
                compare = 2;
        }
        return compare;
    }
}