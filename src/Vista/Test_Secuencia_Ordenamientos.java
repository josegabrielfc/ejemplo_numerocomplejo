/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.Secuencia;

/**
 *
 * @author josga
 */
public class Test_Secuencia_Ordenamientos {

    public static void main(String[] args) {
        
        Secuencia secuencia = new Secuencia(10);
        int[] array = {13, 7, 5, 42, 14, 38, 44, 12, 16, 9};
        ordenar_Seleccion sort = new ordenar_Seleccion();        
        
        sort.printArray(array);

    }

    public void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d \t", array[i]);
        }
        System.out.println();
    }

}
