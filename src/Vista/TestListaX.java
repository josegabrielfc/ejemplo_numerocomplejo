package Vista;

import ufps.util.colecciones_seed.ListaX;

class TestListaX {

    public static void main(String[] args) {
        int num1, num2;
        int copy1, copy2;
        copy1 = num1 = (int) (Math.random() * (System.currentTimeMillis() / 1000));
        copy2 = num2 = (int) (Math.random() * (System.currentTimeMillis() / 1000));
        
        ListaX<Integer> l1, l2;
        
        l1 = new ListaX<>();
        l2 = new ListaX<>();
        while (num1 > 0) {
            l1.append(num1 % 10);
            num1 /= 10;
        }
        while (num2 > 0) {
            l2.append(num2 % 10);
            num2 /= 10;
        }
        System.out.println(copy1);
        System.out.println(l1);
        System.out.println(copy2);
        System.out.println(l2);
        // ¿Ambas tienen repetidos?
        System.out.println(!(l1.isUnique() || l2.isUnique()));
        // Ambas no contienen repetidos?
        System.out.println(l1.isUnique() && l2.isUnique());
        //Eliminar repetidos de l1
        l1.uniqueElements();
        System.out.println(l1);
        // Son iguales?
        System.out.println(l1.compareTo(l2));
        //Unir l1 con l2 en l3
        ListaX<Integer> l3 = l1.union(l2);
        System.out.println(l3);
        //Unir l1 con l2 en l4 y ordenar ascendentemente
        ListaX<Integer> l4 = l1.sort(l2);
        System.out.println(l4);
        // Intersección
        ListaX<Integer> l5 = l1.interseccion(l2);
        System.out.println(l5);
    }
}
