/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author josga
 */
public class ListasCD_TestConcatenar {

    public static void main(String[] args) {
        ListaCD<Integer> l = new ListaCD();
        ListaCD<Integer> l2 = new ListaCD();
        ListaCD<Integer> l3 = new ListaCD();
        ListaCD<Integer> l4 = new ListaCD();


        /*  Prueba concatenar 2 listas
            L3=<4,5> y L4=<1,2,3> 
         */
        l3.insertarInicio(5);
        l3.insertarInicio(4);

        l4.insertarFin(1);
        l4.insertarFin(2);
        l4.insertarFin(3);

        l.insertarInicio(5);
        l.insertarInicio(4);
        l.insertarInicio(1);
        l.insertarFin(7);
        l.insertarFin(8);
        l.insertarFin(9);

        l2.insertarInicio(3);
        l2.insertarInicio(6);
        l2.insertarInicio(9);

        System.out.println("Mi lista1 es:\n" + l.toString() + "\n");
        System.out.println("Mi lista2 es:\n" + l2.toString() + "\n");
        System.out.println("Mi lista3 es:\n" + l3.toString() + "\n");
        System.out.println("Mi lista4 es:\n" + l4.toString() + "\n\n");

        try {
            l3.concatenar(l4); // 2 Listas concatenadas
            System.out.println("La lista 3 y 4 concatenada es: " + l3);

            l.concatenar(l2, 5); // 2 Listas concatenadas en una posición exacta
            System.out.println("La lista 1 y 2 concatenada en la posición especificada es: " + l);
        } catch (Exception e) {
            System.err.println(e);
        }

        System.out.println("\n\nEl tamaño de la Lista1 es: " + l.getTamanio());
        System.out.println("El tamaño de la Lista2 es: " + l2.getTamanio());
        System.out.println("El tamaño de la Lista3 es: " + l3.getTamanio());
        System.out.println("El tamaño de la Lista4 es: " + l4.getTamanio());
    }

}
