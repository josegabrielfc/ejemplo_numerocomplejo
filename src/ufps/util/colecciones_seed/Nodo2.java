/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author josga
 */
public class Nodo2<T> {

    private T info;
    private Nodo2<T> sig;

    public Nodo2(T info) {
        this.info = info;
    }

    public void setSig(Nodo2<T> sig) {
        this.sig = sig;
    }

    public T getInfo() {
        return this.info;
    }

    public Nodo2<T> getSig() {
        return this.sig;
    }

    public int compareTo(Nodo2<T> B) {
        if (info == B.getInfo()) {
            return 0;
        } else if (info.toString().compareTo(B.getInfo().toString()) < 0) {
            return -1;
        } else {
            return 1;
        }
    }

    public String toString() {
        return "[" + info + "]->" + (sig != null ? sig : "");
    }
}
