/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josga
 */
public class ListaX<T> extends ListaS {

    private Nodo2<T> head;

    public void append(T info) {
        Nodo2 newNodo = new Nodo2(info);
        if (head == null) {
            head = newNodo;
        } else {
            Nodo2 aux = head;
            while (aux.getSig() != null) {
                aux = aux.getSig();
            }
            aux.setSig(newNodo);
        }
    }

    public void uniqueElements() {
        if (head == null) {
            return;
        }
        Nodo2 aux = head, last = head;
        Set<T> s = new LinkedHashSet<>();
        s.add(head.getInfo());
        aux = aux.getSig();
        while (aux != null) {
            if (s.contains(aux.getInfo())) {
                //Verificar si ya está marcado un elemento
                last.setSig(aux.getSig());
                aux = aux.getSig();
                continue;
            } else {
                s.add((T) aux.getInfo());
            }
            last = last.getSig();
            aux = aux.getSig();
        }
    }

    public boolean isUnique() {
        if (head == null) {
            return true;
        }
        Nodo2 aux = head, last = head;
        Set<T> s = new LinkedHashSet<>();
        s.add(head.getInfo());
        aux = aux.getSig();
        while (aux != null) {
            if (s.contains(aux.getInfo())) {
                //Verificar si ya está marcado un elemento
                return false;
            } else {
                s.add((T) aux.getInfo());
            }
            last = last.getSig();
            aux = aux.getSig();
        }

        return true;
    }

    public ListaX<T> union(ListaX<T> B) {
        ListaX<T> ans = new ListaX<>();
        Nodo2 it1 = this.head, it2 = B.head;
        while (it1 != null) {
            ans.append((T) it1.getInfo());
            it1 = it1.getSig();
        }
        while (it2 != null) {
            ans.append((T) it2.getInfo());
            it2 = it2.getSig();
        }
        return ans;
    }

    public ListaX<T> sort(ListaX<T> B) {
        ListaX<T> ans = new ListaX<>();
        Nodo2 it1 = this.head, it2 = B.head;

        while (it1 != null) {
            if (ans.head == null) {
                ans.append((T) it1.getInfo());
                it1 = it1.getSig();
                continue;
            }
            Nodo2 aux = ans.head, last = ans.head;
            Nodo2 nuevo = new Nodo2(it1.getInfo());
            aux = aux.getSig();
            if (aux == null) {
                if (last.compareTo(nuevo) > 0) {
                    nuevo.setSig(last);
                    ans.head = nuevo;
                } else {
                    last.setSig(nuevo);
                }
                it1 = it1.getSig();
                continue;
            }
            while (aux != null) {
                if (it1.compareTo(aux) <= 0) {
                    nuevo.setSig(aux);
                    if (last.getSig() != null) {
                        last.setSig(nuevo);
                    } else {
                        ans.head = nuevo;
                    }
                    break;
                }
                aux = aux.getSig();
                last = last.getSig();
            }
            if (aux == null) {
                last.setSig(nuevo);
            }
            it1 = it1.getSig();
        }
        /*
		while(it2 != null){
			if(ans.head == null){
				ans.append((T) it2.getInfo());
				it2 = it2.getSig();
				continue;
			}
			Nodo aux = ans.head, last = ans.head;
			Nodo nuevo = new Nodo(it2.getInfo());
			while( aux != null ){
				if(it2.compareTo(aux) <= 0){
					nuevo.setSig(aux);
					if(last.getSig() != null) last.setSig(nuevo);
					else ans.head = nuevo;
					break;
				}
				aux = aux.getSig();
				last = last.getSig();
			}
			if(aux == null) last.setSig(nuevo);
			it2 = it2.getSig();
		}
         */
        return ans;
    }

    public ListaX<T> interseccion(ListaX<T> B) {
        ListaX<T> inter = new ListaX<>();
        Nodo2 it1 = this.head, it2 = B.head;
        while (it1 != null) {
            inter.append((T) it1.getInfo());
            it1 = it1.getSig();
        }
        while (it2 != null) {
            inter.append((T) it2.getInfo());
            it2 = it2.getSig();
        }
        return B;
    }

    public boolean compareTo(ListaX<T> B) {
        Nodo2 it1 = this.head, it2 = B.head;
        while (it1 != null && it2 != null) {
            if (it1.getInfo() != it2.getInfo()) {
                return false;
            }
            it1 = it1.getSig();
            it2 = it2.getSig();
        }
        if (it1 == null ^ it2 == null) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return head + "NULL";
    }

}
